﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace GetRich.Model
{
    public class YahooFinance
    {
        public string EndPoint { get; set; } = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/market/v2/get-quotes?region=US&symbols=";
        public string Key { get; set; }

        public YahooFinance( string key)
        {
            this.Key = key;
        }

        public async Task<JObject> GetQuota(List<string> tickers)
        {
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri( EndPoint + HttpUtility.UrlEncode(string.Join(",", tickers))),
                Headers =
                {
                    { "x-rapidapi-key", Key },
                    { "x-rapidapi-host", "apidojo-yahoo-finance-v1.p.rapidapi.com" },
                },
            };
            using (var response = await client.SendAsync(request))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                JObject o = JObject.Parse(body);;
                return o;
            }
        }

        public async Task<Dictionary<string, double>> GetRegularMarketPrice(List<string> tickers)
        {
            JObject o = await GetQuota(tickers);

            Dictionary<string, double> ret = new Dictionary<string, double>();

            if (o == null)
                return ret;

            foreach (var ikd in o.SelectToken("quoteResponse.result"))
            {
                string price = (string)ikd.SelectToken("regularMarketPrice");
                string symbol = (string)ikd.SelectToken("symbol");
                
                ret.Add(symbol ?? string.Empty, Double.Parse(price ?? string.Empty));
            }

            return ret;
        }
    }
}