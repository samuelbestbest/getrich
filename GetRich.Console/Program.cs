﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GetRich.Model;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;

namespace GetRich
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json").Build();
            
            string key = config["x-rapidapi-key-yahoo"];

            YahooFinance f = new YahooFinance(key);
            List<string> lst = new List<string>() { "AMC","MSFT"};
            foreach (var dic in await f.GetRegularMarketPrice(lst))
            {
                Console.WriteLine( dic.Key + "=" + dic.Value);
            }
        }
    }
}